<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/15/14
 * Time: 10:02 AM
 */

namespace Smorken\AuthController;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Smorken\Ext\Controller\BaseController;

class AuthController extends BaseController
{

    use AuthenticatesUsers, ValidatesRequests;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->master = config('authcontroller.layout', 'smorken/views::layouts.master');
        $this->middleware('guest', ['except' => 'logout']);
        $mw = config('authcontroller.ssl_middleware', 'force.ssl');
        if (app()->bound($mw)) {
            $this->middleware($mw);
        }
        parent::__construct();
    }

    public function getHelp()
    {
        return view('smorken/authcontroller::auth.help');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        $err = 'Please enter a username and password.';
        try {
            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }
            $err = 'Invalid username or password.';
        } catch (\Smorken\Auth\Contracts\Exception $e) {
            \Log::info($e);
            $err = $e->render();
        } catch (\Exception $e) {
            \Log::error($e);
            $err = 'There was an error connecting to the authentication provider.';
        }
        $this->incrementLoginAttempts($request);
        $this->sendFailedLoginResponseWithMessage($request, $err);
    }

    public function showLoginForm()
    {
        return view('smorken/authcontroller::auth.login');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
    protected function sendFailedLoginResponseWithMessage(Request $request, $message)
    {
        throw ValidationException::withMessages(
            [
                $this->username() => [$message],
            ]
        );
    }
}
