<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/3/15
 * Time: 9:43 AM
 */

namespace Smorken\AuthController\Middleware;

class ForceSsl extends \Smorken\Ext\Http\Middleware\ForceSsl
{

    // kept for backwards compatibility
}
