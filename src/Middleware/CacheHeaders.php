<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 9/18/17
 * Time: 7:01 AM
 */

namespace Smorken\AuthController\Middleware;

class CacheHeaders
{

    public function handle($request, \Closure $next)
    {
        $response = $next($request);
        $response->headers->set('Cache-Control', 'no-cache, no-store, private, max-age=0');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '-1');
        return $response;
    }
}