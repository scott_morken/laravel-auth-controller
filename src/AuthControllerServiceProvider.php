<?php namespace Smorken\AuthController;

use Illuminate\Support\ServiceProvider;

class AuthControllerServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootViews();
        $this->bootConfig();
        $this->loadRoutes();
    }

    protected function bootViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/authcontroller');
        $this->publishes(
            [__DIR__ . '/../views' => base_path('resources/views/vendor/smorken/authcontroller'),],
            'views'
        );
    }

    protected function bootConfig()
    {
        $config = __DIR__ . '/../config/authcontroller.php';
        $this->mergeConfigFrom($config, 'authcontroller');
        $this->publishes([$config => config_path('authcontroller.php')], 'config');
    }

    public function loadRoutes()
    {
        $routefile = __DIR__ . '/../routes.php';
        if (file_exists($routefile)) {
            include $routefile;
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

    }
}
