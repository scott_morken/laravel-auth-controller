<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/15/14
 * Time: 9:59 AM
 */
return [
    'ssl_middleware' => 'force.ssl',
    'controller'     => \Smorken\AuthController\AuthController::class,
    'layout'         => 'smorken/views::layouts.master',
];
