<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 2/9/18
 * Time: 8:12 AM
 */

namespace Tests\Smorken\AuthController\E2E;

use Illuminate\Contracts\Auth\Guard;
use Mockery as m;

class AuthControllerTest extends \Orchestra\Testbench\BrowserKit\TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testLoginRedirectsIfAuthenticated()
    {
        \Route::get(
            '/home',
            function () {
                return 'Hello';
            }
        );
        $guard = $this->getGuard();
        $guard->shouldReceive('check')//pre auth
              ->andReturn(true);
        $this->visit('/auth/login')
             ->see('Hello');
    }

    public function testLoginShowsForm()
    {
        $this->visit('/auth/login')
             ->see('name="username"')
             ->see('name="password"');
    }

    public function testLoginSuccessful()
    {
        \Route::get(
            '/',
            function () {
                return 'Hello';
            }
        );
        $guard = $this->getGuard();
        $guard->shouldReceive('check')//pre auth
              ->andReturn(false);
        $guard->shouldReceive('check')//post auth redirect
              ->andReturn(true);
        $guard->shouldReceive('attempt')
              ->once()
              ->with(['username' => 'foobar', 'password' => 'bizbuz'], false)
              ->andReturn(true);
        $user = m::mock('User');
        $guard->shouldReceive('user')
              ->once()
              ->andReturn($user);
        $this->visit('/auth/login')
             ->type('foobar', 'username')
             ->type('bizbuz', 'password')
             ->press('login')
             ->see('Hello');
    }

    public function testLoginWithAuthExceptionShowsDefaultMessage()
    {
        $guard = $this->getGuard();
        $guard->shouldReceive('check')
              ->andReturn(false);
        $guard->shouldReceive('attempt')
              ->once()
              ->with(['username' => 'foobar', 'password' => 'bizbuz'], false)
              ->andThrow(new \Smorken\Auth\Exceptions\AuthException('Foo Exception'));
        $this->visit('/auth/login')
             ->type('foobar', 'username')
             ->type('bizbuz', 'password')
             ->press('login')
             ->dontSee('username field is required')
             ->dontSee('password field is required')
             ->see('There was an authentication error');
    }

    public function testLoginWithInvalidPassword()
    {
        $guard = $this->getGuard();
        $guard->shouldReceive('check')
              ->andReturn(false);
        $guard->shouldReceive('attempt')
              ->once()
              ->with(['username' => 'foobar', 'password' => 'bizbuz'], false)
              ->andReturn(false);
        $this->visit('/auth/login')
             ->type('foobar', 'username')
             ->type('bizbuz', 'password')
             ->press('login')
             ->dontSee('username field is required')
             ->dontSee('password field is required')
             ->see('Invalid username or password');
    }

    public function testLoginWithNoDataIsValidationError()
    {
        $this->visit('/auth/login')
             ->press('login')
             ->see('username field is required')
             ->see('password field is required');
    }

    public function testLoginWithNoPasswordIsValidationError()
    {
        $this->visit('/auth/login')
             ->type('foobar', 'username')
             ->press('login')
             ->dontSee('username field is required')
             ->see('password field is required');
    }

    public function testThrottle()
    {
        $guard = $this->getGuard();
        $guard->shouldReceive('check')
              ->andReturn(false);
        $guard->shouldReceive('attempt')
              ->with(['username' => 'foobar', 'password' => 'bizbuz'], false)
              ->andReturn(false);
        $this->visit('/auth/login')
             ->type('foobar', 'username')
             ->type('bizbuz', 'password')
             ->press('login')
             ->type('foobar', 'username')
             ->type('bizbuz', 'password')
             ->press('login')
             ->type('foobar', 'username')
             ->type('bizbuz', 'password')
             ->press('login')
             ->type('foobar', 'username')
             ->type('bizbuz', 'password')
             ->press('login')
             ->type('foobar', 'username')
             ->type('bizbuz', 'password')
             ->press('login')
             ->type('foobar', 'username')
             ->type('bizbuz', 'password')
             ->press('login')
             ->see('Too many login attempts. Please try again in');
    }

    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);
        $app['config']->set('authcontroller.layout', 'smorken/authcontroller::test.layout');
    }

    protected function getGuard()
    {
        $guard = m::mock(Guard::class)
                  ->makePartial();
        \Auth::shouldReceive('guard')
             ->andReturn($guard);
        return $guard;
    }

    protected function getPackageProviders($app)
    {
        return [
            \Smorken\AuthController\AuthControllerServiceProvider::class,
            \Smorken\Views\ViewsServiceProvider::class,
        ];
    }
}
