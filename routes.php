<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/15/14
 * Time: 9:54 AM
 */
\Route::group(
    ['prefix' => config('authcontroller.prefix', 'auth'), 'middleware' => ['web']],
    function () {
        \Route::get(
            'login',
            [
                'uses' => config('authcontroller.controller') . '@showLoginForm',
                'as'   => 'login',
            ]
        );
        \Route::post(
            'login',
            [
                'uses' => config('authcontroller.controller') . '@login',
            ]
        );
        \Route::get(
            'logout',
            [
                'uses' => config('authcontroller.controller') . '@logout',
                'as'   => 'logout',
            ]
        );
        \Route::get(
            'help',
            [
                'uses' => config('authcontroller.controller') . '@getHelp',
                'as'   => 'login_help',
            ]
        );
    }
);
