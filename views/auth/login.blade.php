@if (isset($master))
    @extends($master)
@endif
@section('content')
    <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="text" name="username" id="username" value="" placeholder="MEID"
               autofocus required maxlength="64" class="form-control"/>
        <input type="password" name="password" id="password" value="" placeholder="password" maxlength="64"
               class="form-control" autocomplete="off" required/>
        <button type="submit" name="login" class="btn btn-primary btn-block btn-lg">Login</button>
        <div style="margin-top: .5em;">
            <a href="{{ action($controller . '@getHelp') }}" title="Help logging in">
                Difficulty logging in?
            </a>
        </div>
    </form>
@stop
