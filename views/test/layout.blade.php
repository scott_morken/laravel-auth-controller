<!DOCTYPE html>
<head>
</head>
<body>
<div class="container">
    @include('smorken/views::partials._error_bag')
    <div id="content">
        @yield('content')
    </div>
</div>
</body>
</html>
